.PHONY: Fluids.log

FILES :=                              \
    Fluids.html                      \
    Fluids.log                       \
    Fluids.py                        \
    RunFluids.in                     \
    RunFluids.out                    \
    RunFluids.py                     \
    TestFluids.out                   \
    TestFluids.py					  \


ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
    DOC := docker run -it -v $$(PWD):/usr/cs330e -w /usr/cs330e fareszf/python
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
    DOC := docker run -it -v /$$(PWD):/usr/cs330e -w //usr/cs330e fareszf/python
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
    DOC := docker run -it -v $$(PWD):/usr/cs330e -w /usr/cs330e fareszf/python
endif

Fluids.html: fluids_solver.py
	$(PYDOC) -w fluids_solver

Fluids.log:
	git log > fluids_solver.log

RunFluids.tmp: RunFluids.in RunFluids.out RunFluids.py
	$(PYTHON) RunFluids.py < RunFluids.in > RunFluids.tmp
	diff --strip-trailing-cr RunFluids.tmp RunFluids.out

TestFluids.tmp: TestFluids.py
	$(COVERAGE) run    --branch TestFluids.py >  TestFluids.tmp 2>&1
	$(COVERAGE) report -m                      >> TestFluids.tmp
	cat TestFluids.tmp

check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  RunFluids.tmp
	rm -f  TestFluids.tmp
	rm -rf __pycache__
	rm -rf cs330e-Fluids-tests
	
docker:
	$(DOC)
	
config:
	git config -l

format:
	$(AUTOPEP8) -i Fluids.py
	$(AUTOPEP8) -i RunFluids.py
	$(AUTOPEP8) -i TestFluids.py

scrub:
	make clean
	rm -f  Fluids.html
	rm -f  Fluids.log

status:
	make clean
	@echo
	git branch
	git remote -v
	git status
	
versions:
	which       $(AUTOPEP8)
	$(AUTOPEP8) --version
	@echo
	which       $(COVERAGE)
	$(COVERAGE) --version
	@echo
	which       git
	git         --version
	@echo
	which       make
	make        --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which       $(PYLINT)
	$(PYLINT)   --version
	@echo
	which        $(PYTHON)
	$(PYTHON)    --version

test: Fluids.html Fluids.log RunFluids.tmp TestFluids.tmp check