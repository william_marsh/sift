from CoolProp.CoolProp import PropsSI
from CoolProp.CoolProp import PhaseSI
import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

#def compressible_diameter()
    
#def compressible_flowrate()

'''
Input is reynolds number (dimensionless), diameter in m, and roughness in m
Output is the friction_factor implicitly solved using the colebrook implicit equation
'''
def colebrook_friction_factor(reynolds, diameter, roughness):
    x = lambda f: (2 * math.log10((roughness / (3.7 * diameter)) + (2.51 / (reynolds * math.sqrt(f))))) + (1 / math.sqrt(f))
    friction_factor = fsolve(x, 0.001)[0]
    return friction_factor
'''
Input is pressure (Pa), temperature (K), velocity (m/s), diameter (m), fluid name as a string
Output is the reynolds number solved using rho * v * d / mu
'''
def reynolds_number(pressure, temperature, velocity, diameter, fluid):
    density = PropsSI('D','P',pressure,'T',temperature,fluid) #kg/m^3
    viscosity = PropsSI('V','P',pressure,'T',temperature,fluid) #kg/m^3
    return (density * velocity * diameter) / viscosity

'''
Input is input and output pressure (psi), temperature (kelvin), diameter (in), length (m), roughness (m), fluid as a string and a guess for flowrate
Output is the flowrate in kg/s. Solved by iterating through darcy weisbach
'''
def incompressible_pipe_friction_loss(input_pressure, output_pressure, temperature, diameter, length, roughness, fluid, flow_rate_guess=1):
    
    delta_pressure = (input_pressure - output_pressure)* 6894.76 #psi to pascals, pressure drop
    av_pressure = ((input_pressure + output_pressure)/2)* 6894.76 #psi to pascals, averaging pressure across drop

    if PhaseSI('P',av_pressure,'T',temperature, fluid) != 'liquid':
        return -1

    area = math.pi * (((diameter/2) * 0.0254)**2)
    diameter = diameter * 0.0254 #inches to m
    density = PropsSI('D','P',av_pressure,'T',temperature,fluid) #kg/m^3
    #Density is slightly incorrect since delta_pressure not pressure is used but since this is for incompressible fluids this should have negligible effect
    velocity = flow_rate_guess / (density * area)
    fun = lambda velocity: (colebrook_friction_factor(reynolds_number(av_pressure,temperature,velocity,diameter, fluid),diameter, roughness) 
                                            * 0.5 * (density) * (velocity**2) * (length / diameter)) - delta_pressure

    velocity_solution = fsolve(fun, 100)[0]
    return velocity_solution * density * area

'''
solves for the heat transfer coefficient of forced convection around a cylinder
Uses the forced convection cylinder Churchill Bernstein correlation for Nusselts number
surr = surrounding, temp = temperature, cylin = cylinder, htc = heat transfer coefficient 
the characteristic length for a cylinder is the external diameter. Fluid properties are evaluated at film temp
Valid as long as Reynolds*Prandtl number is > 0.2
Input is surr pressure, surface temp, surr temp, velocity, diameter and surr fluid
Ouptut is the heat transfer coefficient
'''
def forced_convec_htc_cylin(surr_pressure, surface_temp, surr_temp, velocity, diameter, surr_fluid):
    film_temp = (surr_temp + surface_temp) / 2
    Re = reynolds_number(surr_pressure, film_temp, velocity, diameter, surr_fluid)
    Pr = PropsSI('PRANDTL', 'P', surr_pressure, 'T', film_temp, surr_fluid)

    if Re*Pr < 0.2:
        return -1

    Nu = 0.3 + (((0.62 * (Re**0.5) * (Pr**(1/3))) /
                 ((1 + ((0.4 * Pr) ** (2/3))) ** (1/4))
                 ) * ((1 + ((Re/282000) ** (5/8))) ** (4/5)))
    k = PropsSI('L', 'P', surr_pressure, 'T', film_temp, surr_fluid)
    return (k * Nu) / diameter

'''
Input is diameter (m), height (m)
Output is surface area (m^2)
'''
def cylin_surface_area(diameter, height):
    radius = diameter/2
    return ((2 * math.pi * height * radius) + (2 * math.pi * (radius ** 2)))

'''
Input is input and output pressure (Pa), temperature (K), sum_of_K, fluid name and a guess for velocity (optional)
Output is the flowrate in kg/s. Solved by iterating through K resistance equation from Crane
IN DEVELOPMENT
'''
def incompressible_velocity(input_pressure, output_pressure, temperature, sum_of_K, fluid, velocity_guess=1):
    delta_pressure = (input_pressure - output_pressure)* 6894.76 #psi to pascals, pressure drop
    av_pressure = ((input_pressure + output_pressure)/2)* 6894.76 #psi to pascals, averaging pressure across drop

    if PhaseSI('P',av_pressure,'T',temperature, fluid) != 'liquid':
        return -1
    
    density = PropsSI('D','P',av_pressure,'T',temperature,fluid) #kg/m^3
    #Density is slightly incorrect since delta_pressure not pressure is used but since this is for incompressible fluids this should have negligible effect
    fun = lambda velocity_guess: (0.5 * density * (velocity_guess ** 2) * sum_of_K) - delta_pressure
    velocity_solution = fsolve(fun, 100)[0]
    return velocity_solution 
