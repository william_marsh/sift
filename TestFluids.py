#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestFluids.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from fluids_solver import colebrook_friction_factor, reynolds_number, incompressible_pipe_friction_loss, forced_convec_htc_cylin, cylin_surface_area

# -----------
# TestFluids
# -----------


class TestFluids (TestCase):

    def setUp(self):
        self.error_range = 0.1

    # ----
    # colebrook friction factor
    # Input is reynolds number (dimensionless), diameter in m, and roughness in m
    # Using https://www.ajdesigner.com/php_colebrook/colebrook_equation.php#ajscroll to check
    # ----

    def test_colebrook_1(self):
        friction_factor = colebrook_friction_factor(10000, 0.021844, 0.0005)
        self.assertAlmostEqual(friction_factor, 0.05464, None, None, self.error_range*friction_factor)

    def test_colebrook_2(self):
        friction_factor = colebrook_friction_factor(100000, 0.01, 0.00001)
        self.assertAlmostEqual(friction_factor, 0.02217, None, None, self.error_range*friction_factor)

    def test_colebrook_3(self):
        friction_factor = colebrook_friction_factor(500, 0.04, 0.00001)
        self.assertAlmostEqual(friction_factor, 0.08137, None, None, self.error_range*friction_factor)

    # ----
    # reynolds
    # Input is pressure (Pa), temperature (K), velocity (m/s), diameter (m), fluid name as a string
    # Using https://www.omnicalculator.com/physics/reynolds-number to check
    # ----

    def test_reynolds_1(self):
        reynolds = reynolds_number(1000000, 300, 100, 0.01, 'Helium')
        self.assertAlmostEqual(reynolds, 80012, None, None, self.error_range*reynolds)

    def test_reynolds_2(self):
        reynolds = reynolds_number(30000, 350, 10, 0.1, 'Water')
        self.assertAlmostEqual(reynolds, 16326, None, None, self.error_range*reynolds)

    def test_reynolds_3(self):
        reynolds = reynolds_number(999999, 70, 20, 0.01, 'Oxygen')
        self.assertAlmostEqual(reynolds, 660548, None, None, self.error_range*reynolds)

    # ----
    # darcy weisbach incompressible flow pipe solver
    # Input is input and output pressure (psi), temperature (kelvin), diameter (in), length (m), roughness (m), fluid as a string
    # Using https://www.omnicalculator.com/physics/darcy-weisbach to check
    # ----

    def test_incompressible_friction_1(self):
        friction_loss = incompressible_pipe_friction_loss(159.037738, 14, 300, 0.4, 10, 0.00005, 'Water')
        self.assertAlmostEqual(friction_loss, 0.6626984082813686, None, None, self.error_range*friction_loss)

    def test_incompressible_friction_2(self):
        friction_loss = incompressible_pipe_friction_loss(159.037738, 14, 300, 0.4, 10, 0.00005, 'Helium')
        self.assertAlmostEqual(friction_loss, -1, None, None, self.error_range*friction_loss)

    def test_incompressible_friction_3(self):
        friction_loss = incompressible_pipe_friction_loss(1014, 14, 70, 0.86, 15, 0.00005, 'Oxygen') #psi and inches
        self.assertAlmostEqual(friction_loss, 11.998829304004609, None, None, self.error_range*friction_loss)

    # ----
    # heat transfer coefficient of forced convection around a cylinder solver using Churchil Bernstein correlation
    # Input is surr pressure, surface temp, surr temp, velocity, diameter and surr fluid
    # Using https://www.fxsolver.com/browse/formulas/Churchill%E2%80%93Bernstein+Equation to check
    # ----

    def test_forced_convec_htc_cylin_1(self):
        htc = forced_convec_htc_cylin(101325, 200, 300, 50, 0.381, 'AIR') #0.381m is trel copv diameter
        self.assertAlmostEqual(htc, 119, None, None, self.error_range*htc)

    def test_forced_convec_htc_cylin_2(self):
        htc = forced_convec_htc_cylin(101325, 300, 300, 0.01, 0.0001, 'AIR')
        self.assertAlmostEqual(htc, -1, None, None, self.error_range*htc)

    # ----
    # Input is diameter and height. Solves for cylinder surface area
    # Using https://www.omnicalculator.com/math/surface-area-of-cylinder to check
    # ----

    def test_cylin_surface_area_1(self):
        area = cylin_surface_area(0.1, 1)
        self.assertAlmostEqual(area, 0.32987, None, None, self.error_range*area)

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestFluids.py >  TestFluids.out 2>&1


$ cat TestFluids.out
.......
----------------------------------------------------------------------
Ran 21 tests in 0.043s
OK


$ coverage report -m                   >> TestFluids.out



$ cat TestFluids.out
.......
----------------------------------------------------------------------
Ran 21 tests in 0.043s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          33      2     16      2    92%   36, 39
TestFluids.py      94      0      0      0   100%
------------------------------------------------------------
TOTAL              127      2     16      2    97%
"""
